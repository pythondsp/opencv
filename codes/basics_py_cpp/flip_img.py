# flip_img.py

import cv2
import numpy as np 


# read image
img = cv2.imread("images/shapes.jpg")
(h, w) = img.shape[:2] # height and width of image
cv2.imshow("Shapes", img) # display image

# flip horizontal
flip_horizontal = cv2.flip(img, 0)
cv2.imshow("Horizontal Flip", flip_horizontal) # display image

# flip vertical
flip_vertical = cv2.flip(img, 1)
cv2.imshow("Vertical Flip", flip_vertical) # display image

# flip vertical and horizontal both
flip_both = cv2.flip(img, -1)
cv2.imshow("Horizontal and Vertical Flip", flip_both) # display image


cv2.waitKey(0)