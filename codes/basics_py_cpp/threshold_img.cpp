// threshold_img.cpp


#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv )
{

    cv::Mat img, thresh_img;  // variable image of datatype Matrix

    img = cv::imread("images/rose.jpg");
    cv::imshow("Rose", img);

    cv::threshold(img, thresh_img, 100, 255, cv::THRESH_BINARY);
    cv::imshow("Threshold Binary", thresh_img);

    cv::threshold(img, thresh_img, 100, 255, cv::THRESH_BINARY_INV);
    cv::imshow("Threshold Binary Inverse", thresh_img);

    cv::waitKey(0);
    return 0;
}