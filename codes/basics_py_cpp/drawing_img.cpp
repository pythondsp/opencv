// drawing_img.cpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv ){

    cv::Mat img, rotate_matrix, rotated; 
    int w, h; // width, height

    // read image
    img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    // width and height of image
    w = img.size().width; 
    h = img.size().height; 

    // draw horizontal line
    cv::line( img, cv::Point( 0, (int)h/2 ), cv::Point( w, (int)h/2), cv::Scalar( 255, 0, 0 ), 3);
    // draw vertical line
    cv::line( img, cv::Point( (int)w/2, 0 ), cv::Point( (int)w/2, h), cv::Scalar( 255, 0, 0 ), 3);

    // draw rectangle
    cv::rectangle( img, cv::Point(5, 10), cv::Point(200, 170), cv::Scalar( 0, 0, 255 ), 3);

    // draw circle 
    // center coordinates (w//2, h//2) and radius (50) are 
    // required to to draw circle. 10 is the line width
    cv::circle(img, cv::Point((int)w/2, (int)h/2), 50, cv::Scalar(0, 0, 0), 10); // black
    cv::circle(img, cv::Point((int)w/2, (int)h/2), 30, cv::Scalar(0, 0, 255), -1); // -1 : filled circle

    cv::imshow("Shapes", img);

    cv::waitKey(0);
    return 0;
}