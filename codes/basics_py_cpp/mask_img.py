# mask_img.py

import cv2
import numpy as np 


# Load two images
img = cv2.imread('images/shapes.jpg')
cv2.imshow("Shapes", img)


# create rectangular frame of size 300x300 with name circle
circle_mask = np.zeros(img.shape[:2], dtype="uint8")# draw circle in rectangular frame

# create a circle at (315, 265) to mask the Yellow circle
cv2.circle(circle_mask, (315, 265), 90, 255, -1)
cv2.imshow("Circle", circle_mask)

# mask the Yellow circle
masked_img = cv2.bitwise_and(img, img, mask=circle_mask)
cv2.imshow("Masked image", masked_img)

cv2.waitKey(0)