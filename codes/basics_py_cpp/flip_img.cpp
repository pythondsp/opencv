// flip_img.cpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv )
{

    cv::Mat img, flip_horizontal, flip_vertical, flip_both;  // variable image of datatype Matrix

    img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    // flip horizontal
    cv::flip(img, flip_horizontal, 0);
    cv::imshow("Horizontal Flip", flip_horizontal); // display image

    // flip vertical
    cv::flip(img, flip_vertical, 1);
    cv::imshow("Vertical Flip", flip_vertical); // display image

    // flip vertical and horizontal both
    cv::flip(img, flip_both, -1);
    cv::imshow("Horizontal and Vertical Flip", flip_both); // display image

    cv::waitKey(0);
    return 0;
}