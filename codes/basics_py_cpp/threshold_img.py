# threshold_img.py

import cv2
import numpy as np 


# read image
img = cv2.imread("images/rose.jpg")
cv2.imshow("Rose", img)

(T, thresh) = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)
cv2.imshow("Threshold Binary", thresh) 


(T, thresh) = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY_INV)
cv2.imshow("Threshold Binary Inverse", thresh) 


cv2.waitKey(0)