# scale_img.py

import cv2
import numpy as np 


# read image
img = cv2.imread("images/lego.jpg")
(h, w) = img.shape[:2] # height and width of image
cv2.imshow("Shapes", img) # display image

# scale by 0.5 in both x and y direction 
scale_img = cv2.resize(img, (w//3, h//3), interpolation = cv2.INTER_CUBIC)
cv2.imshow("Resize Shapes", scale_img) # display image

cv2.imwrite("images/saved_by_opencv.jpg", scale_img)

cv2.waitKey(0)