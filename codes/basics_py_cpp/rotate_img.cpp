// rotate_img.cpp


#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv ){

    cv::Mat img, rotate_matrix, rotated; 
    int w, h; // width, height

    // read image
    img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    // width and height of image
    w = img.size().width; 
    h = img.size().height; 

    // rotation points
    cv::Point2f rotation_center(w/2, h/2);

    rotate_matrix = cv::getRotationMatrix2D(rotation_center, 45, 1.0);

    cv::warpAffine(img, rotated, rotate_matrix, img.size());
    cv::imshow("Rotated by 45 Degrees", rotated);

    cv::waitKey(0);
    return 0;
}