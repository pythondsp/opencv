// translate_img.cpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv )
{

    cv::Mat img, shift_img;  // variable image of datatype Matrix

    // create shift matrix
    float data[6] = { 1, 0, 30, 0, 1, 50 };
    cv::Mat shift_matrix_float = cv::Mat(2, 3, CV_32F, data);
    cout << shift_matrix_float;
    
    // convert to CV_64F format
    cv::Mat shift_matrix = cv::Mat(2, 3, CV_64F);
    shift_matrix_float.convertTo(shift_matrix, CV_64F);
    
    img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    // flip horizontal
    cv::warpAffine(img, shift_img, shift_matrix, img.size());
    cv::imshow("Translate Flip", shift_img); // display image


    cv::waitKey(0);
    return 0;
}