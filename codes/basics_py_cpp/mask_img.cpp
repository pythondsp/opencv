// mask_img.cpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv ){

    cv::Mat img, masked_img; 
    int w, h; // width, height

    // read image
    img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    // width and height of image
    w = img.size().width; 
    h = img.size().height; 


    cv::Mat circle = cv::Mat::zeros( cv::Size(w, h), CV_8UC3);
    cv::circle(circle, cv::Point(315, 265), 90, cv::Scalar(255, 255, 255), -1); // black
    cv::imshow("Circle", circle);

    cv::bitwise_and(img, circle, masked_img); 
    cv::imshow("Masked image", masked_img);

    cv::waitKey(0);
    return 0;
}