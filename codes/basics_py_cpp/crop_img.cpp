// crop_img.cpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 
using namespace cv; 

int main(int argc, char** argv )
{

    cv::Mat img;  // variable image of datatype Matrix
    cv::Mat top_left, top_right, bottom_left, bottom_right; 
    int w, h, cX, cY; 

    img = cv::imread("images/shapes.jpg");

    cout << "(width, height)"<< img.size() << endl;
    cout << "Width : " << img.cols << endl;
    cout << "Height: " << img.rows << endl;

    w = img.size().width; 
    h = img.size().height; 

    cX = (int)w/2; 
    cY = (int)h/2;
    cout << "(cX, cY) = (" << cX << ", " << cY << ")" << endl;


    // (start_x, start_y, len_x, len_y)
    cv::Rect top_left_roi(0, 0, cX, cY);
    top_left = img(top_left_roi);
    cv::imshow("Top left", top_left);

    cv::Rect top_right_roi(cX, 0, cX, cY);
    top_right = img(top_right_roi);
    cv::imshow("Top right", top_right);

    cv::Rect bottom_left_roi(0, cY, cX, cY);
    bottom_left = img(bottom_left_roi);
    cv::imshow("Bottom left", bottom_left);

    cv::Rect bottom_right_roi(cX, cY, cX, cY);
    bottom_right = img(bottom_right_roi);
    cv::imshow("Bottom right", bottom_right);

    // or use above or below, both have same results
    // // (start_x, start_y, len_x, len_y)
    // cv::Rect top_left_roi(0, 0, cX, cY);
    // top_left = img(top_left_roi);
    // cv::imshow("Top left", top_left);

    // cv::Rect top_right_roi(cX, 0, w - cX, cY);
    // top_right = img(top_right_roi);
    // cv::imshow("Top right", top_right);

    // cv::Rect bottom_left_roi(0, cY, cX, h - cY);
    // bottom_left = img(bottom_left_roi);
    // cv::imshow("Bottom left", bottom_left);

    // cv::Rect bottom_right_roi(cX, cY, w - cX, h - cY);
    // bottom_right = img(bottom_right_roi);
    // cv::imshow("Bottom right", bottom_right);


    cv::waitKey(0);

    return(0);
}