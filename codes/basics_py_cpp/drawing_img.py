# drawing_img.py

import cv2
import numpy as np 


# read image
img = cv2.imread("images/shapes.jpg")
(h, w) = img.shape[:2] # height and width of image

# draw blue horizontal and vertical lines at the center of figure
# initial and final point are required to draw line
cv2.line(img,(0, h//2), (w, h//2), (255,0,0), 3) # horizontal line
cv2.line(img,(w//2, 0), (w//2, h), (255,0,0), 3) # vertical line

# draw rectangle 
# top-left corner (5, 10) and bottom-right corner (200, 170) of rectangle
# points are calculated manually
cv2.rectangle(img, (5, 10), (200, 170),(0,0,250),3)


# draw circle 
# center coordinates (w//2, h//2) and radius (50) are 
# required to to draw circle. 10 is the line width
cv2.circle(img, (w//2, h//2), 50 , (0,0,0), 10) # black
cv2.circle(img, (w//2, h//2), 30, (0,0,255), -1) # -1 : filled circle


# draw ellipse 
# center: (w//2, h//2)   
# (major axis, minor axis): (100,50)
# direction of rotation: 0;  where 0 : anticlockwise, 1: clockwise
# start angle and end angle: 0, 360
# color: (0, 255, 0)
# width: 5 (-1 for filled)
cv2.ellipse(img, (w//2, h//2), (100,50), 0, 0, 360, (0, 255, 0), 5)


cv2.imshow("Shapes", img) # display image
cv2.waitKey(0)