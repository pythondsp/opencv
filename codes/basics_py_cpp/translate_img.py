# translate_img.py

import cv2
import numpy as np 

# translation matrix is defined as [1 0 t_x; 0 1 t_y]
# traslate/shift by t_x and t_y respectively

# shift by 30 and 50 in x and y direction respectively 
shift_matrix = np.float32([[1, 0, 30], [0, 1, 50]])


# read image
img = cv2.imread("images/shapes.jpg")
(h, w) = img.shape[:2] # height and width of image
cv2.imshow("Shapes", img) # display image


####### Now perform shift and rotate operation 
shift_img = cv2.warpAffine(img, shift_matrix, (w, h))
cv2.imshow("Shifted Down and Right", shift_img)


cv2.waitKey(0)