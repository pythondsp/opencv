# canny_img.py

import cv2
import numpy as np 


# read image
img = cv2.imread("images/lego.jpg")
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imshow("Lego", gray_img)

# canny edge detection
# choice depends based on data
canny_wide = cv2.Canny(gray_img, 10, 200) # over detection
canny_medium = cv2.Canny(gray_img, 50, 150) # good detection
canny_narrow = cv2.Canny(gray_img, 200, 250) # missing detection

# show the output images
cv2.imshow("Canny (10, 200)", canny_wide) 
cv2.imshow("Canny (50, 150)", canny_medium)
cv2.imshow("Canny (200, 250)", canny_narrow)


cv2.waitKey(0)