# rotate_img.py

import cv2
import numpy as np 

# read image
img = cv2.imread("images/shapes.jpg")
(h, w) = img.shape[:2] # height and width of image
cv2.imshow("Shapes", img) # display image


# first define the point of rotation, e.g. (w/2, h/2) i.e. center of the image
(cX, cY) = (w/2, h/2)
# now define rotation matrix with 45 degree of rotation
rotation_matrix = cv2.getRotationMatrix2D((cX, cY), 45, 1.0)

# rotate and plot the image
rotated = cv2.warpAffine(img, rotation_matrix, (w, h))
cv2.imshow("Rotated by 45 Degrees", rotated)


cv2.waitKey(0)