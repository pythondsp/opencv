# bitwise_img.py

import numpy as np
import cv2
 

# create and display frame of size 300
rectangle = np.zeros((300, 300), dtype = "uint8")
# display empty frame
cv2.imshow("Frame", rectangle)


# draw white rectangle 
cv2.rectangle(rectangle, (20, 20), (280, 280), 255, -1)
cv2.imshow("Rectangle", rectangle)
 

# create rectangular frame of size 300x300 with name circle
circle = np.zeros((300, 300), dtype = "uint8")
# draw circle in rectangular frame
cv2.circle(circle, (150, 150), 150, 255, -1)
cv2.imshow("Circle", circle)


and_img = cv2.bitwise_and(circle, rectangle)
cv2.imshow("And", and_img)



# another example 
rect1 = np.zeros((200, 400), dtype = "uint8")
rect2 = np.zeros((200, 400), dtype = "uint8")

rect1 = cv2.rectangle(rect1, (0, 200), (200, 0), 255, -1)
cv2.imshow("rect1", rect1);
rect2 = cv2.rectangle(rect2, (150, 100), (250, 150), 255, -1)
cv2.imshow("rect2", rect2);

result = cv2.bitwise_and(rect1, rect2);     
cv2.imshow("AND", result);

result = cv2.bitwise_or(rect1, rect2);      
cv2.imshow("OR", result);

result = cv2.bitwise_xor(rect1, rect2);     
cv2.imshow("XOR", result);

result = cv2.bitwise_not(rect2);      
cv2.imshow("rect2 NOT", result);

cv2.waitKey()