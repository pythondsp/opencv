# crop_img.py

import cv2

# read image
img = cv2.imread("images/shapes.jpg")
cv2.imshow("Shapes", img) # display image

# Shape = (width, height, channel);  channel = 3 i.e. B, G, R
print("Image shape: ", img.shape) # Image shape:  (360, 640, 3)


# extract height and width i.e. first two values (360, 640)
(h, w) = img.shape[:2] 
print("Heigth = {}, Width = {}".format(h, w)) # Heigth = 360, Width = 640

#### Pixel values
# print pixel value (B, G, R) at [0, 0]
print("(B G R) = ", img[0, 0]) # (B G R) =  [255 255 255] i.e. white
# print pixel value (B, G, R) at [40, 310]
print("(B G R) = ", img[40, 310]) # (B G R) =  [  0   0 254] i.e. red


### Crop image

# center point of image
# note that we will use the cX and cY as pixel location
# therefore these need to be an integer value, hence // is used
(cX, cY) = (w//2, h//2) 

# top left i.e. 0-to-cY and 0-to-cX
top_left = img[0:cY, 0:cX]
cv2.imshow("Top Left", top_left) # display image 

top_right = img[0:cY, cX:w]
cv2.imshow("Top Right", top_right) # display image 

bottom_left = img[cY:h, 0:cX]
cv2.imshow("Bottom Left", bottom_left) # display image 

bottom_right = img[cY:h, cX:w]
cv2.imshow("Bottom Right", bottom_right) # display image 

cv2.waitKey(0)


### change color for cropped sections
img[cY:h, cX:w] = [255, 0, 0] # bottom right to Blue color
cv2.imshow("Bottom Right", bottom_right) # display image 

# Green + Red = Yellow
img[0:cY, 0:cX] = [0, 255, 255] # Yellow color for top-left
cv2.imshow("Top Left", top_left) # display image 


cv2.waitKey(0)