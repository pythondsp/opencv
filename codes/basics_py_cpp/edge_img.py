# edge_img.py

import cv2
import numpy as np 


# read image
img = cv2.imread("images/lego.jpg")
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
cv2.imshow("Lego", gray_img)

# compute gradients along the X and Y axis, respectively
gX = cv2.Sobel(gray_img, cv2.CV_64F, 1, 0)
gY = cv2.Sobel(gray_img, cv2.CV_64F, 0, 1)
#gX value after sobel conversion -52.0
print("gX value after sobel conversion", gX[100,200])

# gX and gY are decimal number with +/- values
# change these values to +ve integer format
gX = cv2.convertScaleAbs(gX)
# gX value after Absolute scaling 52
gY = cv2.convertScaleAbs(gY)
print("gX value after Absolute scaling", gX[100,200])

# combine the sobel X and Y in single image with equal amount
sobelCombined = cv2.addWeighted(gX, 0.5, gY, 0.5, 0)
 
# show our output images
cv2.imshow("Sobel X", gX)
cv2.imshow("Sobel Y", gY)
cv2.imshow("Sobel Combined", sobelCombined)


cv2.waitKey(0)