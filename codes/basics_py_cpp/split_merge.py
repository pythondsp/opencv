# split_merge.py

# square is of red color: R = 255 (i.e. white), B & G = 0 (i.e. black)

# circle is of yellow color: R & G = 255 (i.e. white), B = 0 (i.e. black)

# triangle is purple: a mix of R & B with different ratio; therefore a different 
# gray-shades for R and B (more of blue therefore lighter-gray shade) will be shown; 
# whereas G = 0 (i.e. black) 

import cv2
import numpy as np 


# read image
img = cv2.imread("images/shapes.jpg")
(h, w) = img.shape[:2] # height and width of image

# split image into BGR 
(B, G, R) = cv2.split(img)

# show B, G, R channels
cv2.imshow("Shapes", img)
cv2.imshow("Blue", B) 
cv2.imshow("Green", G)
cv2.imshow("Red", R)

merge_img = cv2.merge([B, G, R])
cv2.imshow("Merged BGR", merge_img)


cv2.waitKey(0)