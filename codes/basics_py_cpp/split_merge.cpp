// split_merge.cpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 
using namespace cv; 

int main(int argc, char** argv )
{

    cv::Mat img, sum_rgb;  // variable image of datatype Matrix
    img = cv::imread("images/shapes.jpg");
    cv::imshow("Display Image", img);

    // three channel to store b, g, r
    cv::Mat rgbchannel[3];

    // split image
    cv::split(img, rgbchannel);
     
    // plot individual component
    cv::namedWindow("Blue",CV_WINDOW_AUTOSIZE);
    cv::imshow("Red", rgbchannel[0]);
     
    cv::namedWindow("Green",CV_WINDOW_AUTOSIZE);
    cv::imshow("Green", rgbchannel[1]);
     
    cv::namedWindow("Red",CV_WINDOW_AUTOSIZE);
    cv::imshow("Blue", rgbchannel[2]);

    // merge : (input, num_of_channel, output)
    cv::merge(rgbchannel, 3, sum_rgb);
    cv::imshow("Merged", sum_rgb);

    cv::waitKey(0);

    return 0;
}