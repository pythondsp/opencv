// image_arith.cpp


#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 
// using namespace cv; 

int main(int argc, char** argv )
{


    // ############# Various method to define Matrix #################

    // intialize matrix with contant value 80
    cv::Mat matB(3, 3, CV_8UC1, cv::Scalar(80));
    cout << "matB = " << endl << " " << matB << endl << endl;

    // zero matrix
    cv::Mat matZeros = cv::Mat::zeros(3,3, CV_8UC1);
    cout << "matZeros = " << endl << " " << matZeros << endl << endl;
    
    // eye matrix
    cv::Mat matEye = cv::Mat::eye(3, 3, CV_64F);
    cout << "matEye = " << endl << " " << matEye << endl << endl;

    // ones matrix
    cv::Mat matOnes = cv::Mat::ones(3, 3, CV_32F);
    cout << "matOnes = " << endl << " " << matOnes << endl << endl;

    float data[10] = { 221, 23, 9, 104, 51, 65, 76, 48, 210 };
    cv::Mat A = cv::Mat(3, 3, CV_32F, data);
    cout << "A = " << endl << " " << A << endl << endl;

    cv::Mat B(3, 3, CV_8UC1, cv::Scalar(80));
    cout << "B = " << endl << " " << B << endl << endl;

    // convert format
    cv::Mat A_convert = cv::Mat(3, 3, CV_8UC1);
    A.convertTo(A_convert, CV_8UC1);
    cout << "A_convert = " << endl << " " << A_convert << endl << endl;


    // define 3x3 matrix
    cv::Mat matOut = cv::Mat(3, 3, CV_8UC1);

    // ######################## Add/subtract ####################################
    
    // cv::add(A_convert, B, matOut) is not possible due to different data type
    cv::add(A_convert, B, matOut); 
    cout << "A_convert + B = \n" << matOut << endl << endl;

    // subtract
    cv::subtract(A_convert, B, matOut); 
    cout << "A_convert - B = \n" << matOut << endl << endl;


    // ############ Image addtion

    cv::Mat img, add_img, sub_img;  // variable image of datatype Matrix
    
    // read image
    img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    // define new mat with same size as img
    cv::Mat new_pixel = 90 * cv::Mat::ones(img.size(), img.type());

    // add and show
    cv::add(img, new_pixel, add_img);
    cv::imshow("Add image", add_img);

    // subtract and show
    cv::subtract(img, new_pixel, sub_img);
    cv::imshow("Subtract image", sub_img);


    cv::waitKey(0);

    return(0);
}




