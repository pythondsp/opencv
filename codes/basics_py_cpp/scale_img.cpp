// scale_img.cpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv )
{

    cv::Mat img, resize_img;  // variable image of datatype Matrix
    int w, h;

    img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    w = img.size().width; 
    h = img.size().height; 

    cv::resize(img, resize_img, cv::Size((int)w/2, (int)h/2), cv::INTER_CUBIC);
    cv::imshow("Resize Shapes", resize_img);

    cv::waitKey(0);
    return 0;
}