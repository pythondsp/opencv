# load_save_image.py

import cv2

# read image
img = cv2.imread("images/shapes.jpg")

# show image
cv2.imshow("Shapes", img) # Window name -> Shapes

# wait for key before closing the window
cv2.waitKey(0)

# save image
cv2.imwrite("images/saved_by_opencv.jpg", img)