// canny_img.cpp 

#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std; 

int main(int argc, char** argv ){

    cv::Mat img, gray_img; 
    int w, h; // width, height

    // read image
    img = cv::imread("images/lego.jpg");
    cv::imshow("Lego color", img);

    // width and height of image
    w = img.size().width; 
    h = img.size().height; 


    cv::cvtColor(img, gray_img, cv::COLOR_BGR2GRAY);
    cv::imshow("Lego ", gray_img);

    cv::Mat canny_wide, canny_medium, canny_narrow;

    cv::Canny(gray_img, canny_wide, 10, 200);
    cv::Canny(gray_img, canny_medium, 50, 150);
    cv::Canny(gray_img, canny_narrow, 200, 250);

    // show the output images
    cv::imshow("Canny (10, 200)", canny_wide);
    cv::imshow("Canny (50, 150)", canny_medium);
    cv::imshow("Canny (200, 250)", canny_narrow);


    cv::waitKey(0);
    return 0;
}