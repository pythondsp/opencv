#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;

int main(int argc, char **argv)
{
    if( argc != 2)
    {
        cout <<" Usage: ./rotateimage <image-name>.jpg" << std::endl;
        return -1;
    }
  
    // read image
    cv::Mat img = cv::imread(argv[1]);

    // Check for invalid input
    if( img.empty() )
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    // img = cv::imread("images/shapes.jpg");
    cv::imshow("Shapes", img);

    int w, h; // width, height
    // width and height of image
    w = img.size().width;
    h = img.size().height;

    // draw horizontal line
    cv::line( img, cv::Point( 0, (int)h/2 ), cv::Point( w, (int)h/2), cv::Scalar( 255, 0, 0 ), 3);
    // draw vertical line
    cv::line( img, cv::Point( (int)w/2, 0 ), cv::Point( (int)w/2, h), cv::Scalar( 255, 0, 0 ), 3);

    // draw rectangle
    cv::rectangle( img, cv::Point(5, 10), cv::Point(200, 170), cv::Scalar( 0, 0, 255 ), 3);

    // draw circle
    // center coordinates (w//2, h//2) and radius (50) are
    // required to to draw circle. 10 is the line width
    cv::circle(img, cv::Point((int)w/2, (int)h/2), 50, cv::Scalar(0, 0, 0), 10); // black
    cv::circle(img, cv::Point((int)w/2, (int)h/2), 30, cv::Scalar(0, 0, 255), -1); // -1 : filled circle

    cv::imshow("Shapes", img);
    cv::imwrite("/home/root/drawing_img.jpg", img);
    cv::waitKey(0);
    return 0;
}
