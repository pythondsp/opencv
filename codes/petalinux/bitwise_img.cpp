#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;

int main(int argc, char** argv ){

    cv::Mat and_img;

    // create and display frame of size 300 for rectangle and circle
    cv::Mat rectangle(300, 300, CV_8UC1, cv::Scalar(0)); // rectangle
    cv::Mat circle(300, 300, CV_8UC1, cv::Scalar(0)); // circle


    // draw and show rectangle
    cv::rectangle( rectangle, cv::Point(20, 20), cv::Point(280, 280), cv::Scalar( 255 ), -1);
    cv::imshow("Rectangle", rectangle);

    // draw and show circle
    cv::circle(circle, cv::Point(150, 150), 150, cv::Scalar(255), -1); // black
    cv::imshow("Circle", circle);

    // bitwise and operation
    cv::bitwise_and(circle, rectangle, and_img);
    cv::imshow("And", and_img);



    // another example
    cv::Mat rect1 = cv::Mat::zeros( cv::Size(400,200), CV_8UC1);
    cv::Mat rect2 = cv::Mat::zeros( cv::Size(400,200), CV_8UC1);


    rect1( cv::Range(0, 200), cv::Range(0, 200) ) = 255;
    cv::imshow("rect1", rect1);
    cv::imwrite("/home/root/rect1.jpg", rect1); 
    cout << "Image written at /home/root/rect1.jpg " << endl; 

    rect2( cv::Range(100, 150), cv::Range(150, 250) ) = 255;
    cv::imwrite("/home/root/rect2.jpg", rect2); 
    cout << "Image written at /home/root/rect2.jpg " << endl; 
    cv::imshow("rect2", rect2);

    cv::Mat result;

    bitwise_and(rect1, rect2, result);
    cv::imwrite("/home/root/result_and.jpg", result); 
    cv::imshow("AND", result);

    bitwise_or(rect1, rect2, result);
    cv::imwrite("/home/root/result_or.jpg", result); 
    cv::imshow("OR", result);

    bitwise_xor(rect1, rect2, result);
    cv::imwrite("/home/root/result_xor.jpg", result); 
    cv::imshow("XOR", result);

    bitwise_not(rect2, result);
    cv::imwrite("/home/root/not_rect2.jpg", result); 
    cv::imshow("rect2 NOT", result);

    cout << "before waitkey" << endl; 
    cv::waitKey(0);

    return 0;
}
